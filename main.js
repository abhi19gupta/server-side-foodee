var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require('fs');

var porthttp = 8443;
var porthttp_string = '8443'
var ip = '54.254.131.45';


// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();


//Lets load the mongoose module in our program
var mongoose = require('mongoose');

//Lets connect to our database using the DB server URL.
mongoose.connect('mongodb://localhost/my_database_name');

var orderSchema = mongoose.Schema({
    date:{type:Date, default:Date.now},
    orderid:{type: Number, required: true}, 
    restorderid:{type: String},
    restaurant:{type: String},
    restaurantAddress:{type:String}, 
    status:{type: String, required: true, enum: ['Pending', 'Delivered', 'Dispatched', 'RejectedCust', 'RejectedUs']}, 
    username: String, 
    mobno: String, 
    content: [{ID:String,name:String,price:Number,tax:Number,quantity:Number}]
});

var orderNumSchema = mongoose.Schema({
    ordernum: Number
});

/**
 * A model is a class with whtheich we construct documents (rows) which are inserted into the collection (tables).
 * The fisrst argument is  name of the collection (table)
 * */
var Order = mongoose.model('Orders', orderSchema);
var OrderNum = mongoose.model('OrderNum', orderNumSchema);

app.use(express.static('public'));






function addOrder(orderID,rest,restAddr,Status,UserName,MobNo,Content){
	Order.create({orderid:orderID, restaurant:rest, restaurantAddress:restAddr, status:Status, username:UserName, mobno:MobNo, content:Content}, function (err, result) {
    if (err) {
      console.log(err);
    } else {
      console.log('saved successfully:', result);
    }
  })
};

function deleteOrder(orderID){
  Order.remove({ orderid: orderID }, function (err) {
    if (err) return handleError(err);
  });
};

function deleteAllOrders(orderID){
  Order.remove(function (err) {
    if (err) return handleError(err);
  });
};

function updateStatus(orderID,newStatus){
  Order.findOneAndUpdate({orderid:orderID}, { $set: { status: newStatus }}, {new:true}, function(err,result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log('Updated:', result);
    } else {
      console.log('No document(s) found with defined "find" criteria!');
    }
  });
};

function updateRestOrderID(orderID,restOrderID){
  Order.findOneAndUpdate({orderid:orderID}, { $set: { restorderid: restOrderID }}, {new:true}, function(err,result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log('RestOrderID Updated:', result);
    } else {
      console.log('No document(s) found with defined "find" criteria!');
    }
  });
};

function printSingleOrder(orderID){
  Order.find({orderid:orderID},function (err, result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log('PrintSingle:', result);
    } else {
      console.log('No document(s) found with defined "find" criteria!');
    }
  });
};

function printAllOrders(){
  Order.find(function (err, result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log('PrintAll:', result);
    } else {
      console.log('No document(s) found with defined "find" criteria!');
    }
  });
};

function printPendingOrders(){
  Order.find({status:'Pending'},function (err, result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log('Pending Orders:', result);
    } else {
      console.log('No document(s) found with defined "find" criteria!');
    }
  });
};

function printDispatchedOrders(){
  Order.find({status:'Dispatched'},function (err, result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log('Dispatched Orders:', result);
    } else {
      console.log('No document(s) found with defined "find" criteria!');
    }
  });
};

function printDeliveredOrders(){
  Order.find({status:'Delivered'},function (err, result) {
    if (err) {
      console.log(err);
    } else if (result) {
      console.log('Delivered Orders:', result);
    } else {
      console.log('No document(s) found with defined "find" criteria!');
    }
  });
};

function getItems(jsonArr){
  var order = '<table border="1" style="width:100%">\n<tr>\n<th>ItemID</th>\n<th>Name</th>\n<th>Price</th>\n<th>Tax</th>\n<th>Qty</th>\n</tr>';
  for ( var i = 0; i < jsonArr.length; i++ ) {
    order+=('<tr>\n<td width="25%">'+jsonArr[i].ID+"</td>\n");
    order+=('<td width = "35%">'+jsonArr[i].name+"</td>\n");
    if(jsonArr[i].price!=undefined)
      order+=('<td width = "20%">'+jsonArr[i].price.toString()+"</td>");
    else
      console.log("undefined price");
   	if(jsonArr[i].tax!=undefined)
      order+=('<td width = "20%">'+jsonArr[i].tax.toString()+"</td>");
    else
      console.log("undefined tax");
    if(jsonArr[i].quantity!=undefined)
      order+=('<td width = "20%">'+jsonArr[i].quantity.toString()+"</td>");
    else
      console.log("undefined quantity");
  }
  order+="</table>";
  return order
}

function initialize(){
	deleteAllOrders();
	printAllOrders();

	OrderNum.remove(function (err) {
	    if (err) return handleError(err);
	    else{
	    	OrderNum.create({ordernum:0}, function (err, result) {
			    if (err) {
			      console.log(err);
			    } else {
			      console.log('ordernum:', result);
			    }
			  })
	    }
  	});
}

// POST: name=foo&color=red            <-- URL encoding
// POST: {"name":"foo","color":"red"}  <-- JSON encoding

// app.get('/checkOrders', function (req, res) {
//    res.sendFile( __dirname + "/" + "checkOrders.html" );
// })

//WEB
app.get('/viewOrders', function (req, res) {
  //console.log('viewing orders...');
  res.writeHead(200, { 'Content-Type': 'text/html' });

  var html = '<!DOCTYPE html>\n<html>\n<head><meta http-equiv="refresh" content="20"></head><body>\n<table border="1" style="width:100%">\n<tr>\n<th width="15%">Date/Time</th>\n<th width="10%">Order ID</th>\n<th width="10%">Rest Order ID</th>\n<th width="8%">Restaurant</th>\n<th width="8%">RestAddress</th>\n<th width="8%">Status</th>\n<th width="8%">Name</th>\n<th width="9%">Mobile No</th>\n<th width="24%">Order</th>\n</tr>\n'

  var queryType = req.query.typ;
  var queryRest = req.query.rest;


  if(queryType==undefined){
    Order.find(function (err, result) {
      if (err) {
        console.log(err);
      } 
      else if (result) {
        result.forEach(function(order){ //I think foreach is not an asynchronus call, its just like a for loop
          html+=("<tr>\n<td>"+(order.date)+"</td>\n<td>"+(order.orderid).toString()+"</td>\n<td>"+(order.restorderid)+"</td>\n<td>"+(order.restaurant)+"</td>\n<td>"+(order.restaurantAddress)+"</td>\n<td>"+order.status+"</td>\n<td>"+order.username+"</td>\n<td>"+order.mobno+"</td>\n<td>"+getItems(order.content)+"</td>\n</tr>\n");
        });
      } 
      else {
        console.log('getData - no data found');
      }
      
      html+='</table>\n</body>\n</html>';
      res.end(html, 'utf-8');
    });
  }
  else{
  	var query;
  	if(queryRest==undefined)
  		query = {status:queryType}
  	else
  		query = {status:queryType, restaurant:queryRest}
    Order.find(query,function (err, result) {
      if (err) {
        console.log(err);
      } 
      else if (result) {
        result.forEach(function(order){ //I think foreach is not an asynchronus call, its just like a for loop
          html+=("<tr>\n<td>"+(order.date)+"</td>\n<td>"+(order.orderid).toString()+"</td>\n<td>"+(order.restorderid)+"</td>\n<td>"+(order.restaurant)+"</td>\n<td>"+(order.restaurantAddress)+"</td>\n<td>"+order.status+"</td>\n<td>"+order.username+"</td>\n<td>"+order.mobno+"</td>\n<td>"+getItems(order.content)+"</td>\n</tr>\n");
        });
      } 
      else {
        console.log('getData - no data found');
      }
      
      html+='</table><br>\n';      
      if(queryType=='Pending'){
        html+='<form action="http://'+ip+':'+porthttp_string+'/dispatchOrder" method="POST">OurOrderID: <input type="text" name="orderid">RestOrderID: <input type="text" name="restorderid"><input type="submit" value="Dispatched!"></form><br>\n';
        html+='<form action="http://'+ip+':'+porthttp_string+'/rejectCustOrder" method="POST">OrderID: <input type="text" name="orderid"><input type="submit" value="Rejected by customer!"></form><br>';
        html+='<form action="http://'+ip+':'+porthttp_string+'/rejectUsOrder" method="POST">OrderID: <input type="text" name="orderid"><input type="submit" value="Rejected by us!"></form><br>';
        html+='<form action="http://'+ip+':'+porthttp_string+'/deleteOrder" method="POST">OrderID: <input type="text" name="orderid"><input type="submit" value="Delete!"></form><br>';
      }
      else if(queryType=='Dispatched'){
        html+='<form action="http://'+ip+':'+porthttp_string+'/deliverOrder" method="POST">OrderID: <input type="text" name="orderid"><input type="submit" value="Delivered!"></form>'
        html+='<form action="http://'+ip+':'+porthttp_string+'/deleteOrder" method="POST">OrderID: <input type="text" name="orderid"><input type="submit" value="Delete!"></form><br>';
      }
      html+='</body>\n</html>';
      res.end(html, 'utf-8');
    });
  }
})

app.post('/dispatchOrder', urlencodedParser, function (req, res) {
  console.log('dispatching order...');
  updateStatus(req.body.orderid,'Dispatched');
  updateRestOrderID(req.body.orderid,req.body.restorderid);
  res.sendFile( __dirname + "/" + "checkOrders.html" );
})

app.post('/rejectCustOrder', urlencodedParser, function (req, res) {
  console.log('rejectCustOrder...');
  updateStatus(req.body.orderid,'RejectedCust');
  res.sendFile( __dirname + "/" + "checkOrders.html" );
})

app.post('/rejectUsOrder', urlencodedParser, function (req, res) {
  console.log('rejectUsOrder...');
  updateStatus(req.body.orderid,'RejectedUs');
  res.sendFile( __dirname + "/" + "checkOrders.html" );
})

app.post('/deliverOrder', urlencodedParser, function (req, res) {
  console.log('delivering order...');
  updateStatus(req.body.orderid,'Delivered');
  res.sendFile( __dirname + "/" + "checkOrders.html" );
})

app.post('/deleteOrder', urlencodedParser, function (req, res) {
  console.log('deleting order...');
  deleteOrder(req.body.orderid);
  res.sendFile( __dirname + "/" + "checkOrders.html" );
})

//APP
app.get('/getFoodItems', function (req, res) {
  console.log('getFoodItems...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Burger\n95\nPizza\n190\nPasta\n100');
})

app.get('/getRestaurants', function (req, res) {
  console.log('getRestaurants...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("KFC\nDelhi\nClosed\n1 PM\nMcDonald's\nDelhi\nClosed\nthe earliest. Thanks for the overwhelming response!");
})

app.get('/getKFCDelhi', function (req, res) {
  console.log('getMenuKFC...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("Burgers\nChicken\nRollers\nRice-Bowlz\nFries");
})

app.get('/getKFCDelhi/Burgers', function (req, res) {
  console.log('getMenuKFC/Burgers...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("KFC1\nPotato Krisper\n35\n6.19\nKFC7\nOMG Burger\n49\n8.67\nKFC8\nCheezy Crunch\n79\n13.98\nKFC6\nPaneer Zinger\n115\n20.35\nKFC3\nChicken Snacker\n45\n7.96\nKFC4\nChicken Rockin\n85\n15.04\nKFC5\nChicken Zinger\n119\n21.06\n");
})

app.get('/getKFCDelhi/Krushers', function (req, res) {
  console.log('getMenuKFC/Krushers...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("KFC21\nAlphonso Burst - Mini\n49\n12.34\nKFC21a\nAlphonso Burst - Regular\n95\n23.94\nKFC22\nStrawberry storm - Mini\n49\n12.34\nKFC22a\nStrawberry storm - Regular\n95\n23.94\nKFC23\nChoco Lash - Mini\n59\n14.86\nKFC23a\nChoco Lash - Regular\n105\n26.46\nKFC24\nChocopeanut Bolt - Mini\n59\n14.86\nKFC24a\nChocopeanut Bolt - Regular\n105\n26.46\n");
})

app.get('/getKFCDelhi/Chicken', function (req, res) {
  console.log('getMenuKFC/Chicken...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("KFC46\nHot Wings (2 Pc.)\n40\n7.08\nKFC54\nBoneless Strips (3 pc.)\n99\n11.7\nKFC57\nBoneless Strips (6 pc.)\n195\n34.51\nKFC42\nHot and Crispy 2 piece\n155\n27.43\nKFC43\nHot and Crispy 4 piece\n295\n52.21\nKFC44\nHot and Crispy Bucket\n559\n98.94\nKFC45\nPopcorn Chicken - Reg\n59\n10.44\nKFC45a\nPopcorn Chicken - Med\n99\n17.52\nKFC45b\nPopcorn Chicken - Lrg\n149\n26.37\n");
})

app.get('/getKFCDelhi/Rollers', function (req, res) {
  console.log('getMenuKFC/Wings...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("KFC53\nOMG Roller\n59\n10.43\nKFC51\nCheezy Crunch Wrap\n69\n12.21\n");
})

app.get('/getKFCDelhi/Rice-Bowlz', function (req, res) {
  console.log('getMenuKFC/Sides...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("KFC41\nKrispy Potato Rice Bowl\n85\n15.04\nKFC55\nChicken Popcorn Rice Bowl\n95\n16.81\nKFC56\nFiery Grilled Rice Bowl\n125\n22.12\n");
})

app.get('/getKFCDelhi/Fries', function (req, res) {
  console.log('getMenuKFC/Sides...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("KFC61\nFries Reg\n55\n9.73\nKFC62\nFries Med\n70\n12.39\nKFC63\nFries Large\n90\n15.93\n");
})

app.get("/getMcDonald'sDelhi", function (req, res) {
  console.log('getMenuMcD...');
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("Meals\nBurgers\nFries");
})

app.get("/getMcDonald'sDelhi/Meals", function (req, res) {
  console.log("getMcDonald's/Meals...");
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("MCD1\nMcAloo-Tikki Econo-Meal\n119\n21.54\nMCD2\nChicken-McGrill Econo-Meal\n129\n23.35\n");
})

app.get("/getMcDonald'sDelhi/Burgers", function (req, res) {
  console.log("getMcDonald's/Burgers...");
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("MCD21\nMcAloo-Tikki\n32\n5.79\nMCD22\nMcVeggie\n70\n12.67\nMCD23\nMcSpicy Paneer\n120\n21.72\nMCD24\nPizza-McPuff\n25\n4.52\nMCD25\nChicken-McGrill\n46\n8.32\nMCD26\nMcChicken\n89\n16.12\nMCD27\nMcSpicy Chicken\n128\n23.16\n");
})

app.get("/getMcDonald'sDelhi/Fries", function (req, res) {
  console.log("getMcDonald's/Fries...");
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("MCD41\nFrench Fries - Small\n60\n10.86\nMCD42\nFrench Fries - Med\n75\n13.57\nMCD43\nFrench Fries - Large\n95\n17.19\n");
})

app.get("/getVersionAndroid", function (req, res) {
  console.log("version asked...");
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end("3\n3"); // means current is 2 and minimum needed is 1
})

app.post('/postOrder', jsonParser, function (req, res) {

	OrderNum.findOneAndUpdate({},{$inc: { ordernum: 1 }}, {new:true}, function(err,result) {
	    if (err) {
	      console.log(err);
	    } 
	    else if (result) {
			console.log('ordernum',result);
			addOrder(result.ordernum,req.body.rest,req.body.restAddr,'Pending',req.body.username,req.body.mobno,req.body.order);
			printAllOrders();
			// Prepare output in JSON format
			// response = {
			//     usernm:req.body.username,
			//     mob:req.body.mobno,
			//     ord:req.body.order
			// };
			// console.log(response);
			res.end("9717671313");
	    } 
	    else {
	      console.log('No document(s) found with defined "find" criteria!');
	    }
	  });
})


//initialize();
var server = app.listen(porthttp, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)
})
